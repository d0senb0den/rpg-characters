# RPG-Character

This is a simple console application using the programming language c#.
You can create characters and equip armors and weapons to increase their damage and attributes.
You can also display other stats such as name, level and the equipment that you have created for your character.
The reason behind the application was mainly to try out object oriented programming and testing.

## Testing

There are several tests available in the same solution to check if the methods are running as they should.

```CharacterStatsAndLevelTest.cs``` Tests for characters.

```EquipmentTest.cs``` Tests for equipping weapons and armors.

## 
### Feel free to fork this project and try it out! :)
