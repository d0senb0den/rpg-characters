﻿namespace RPGcharacters.Enums;

public enum WeaponTypes
{
    Axe,
    Bow,
    Dagger,
    Hammer,
    Staff,
    Sword,
    Wand
}
