﻿namespace RPGcharacters.Enums;
public enum ArmorTypes
{
    Cloth,
    Leather,
    Mail,
    Plate
}
