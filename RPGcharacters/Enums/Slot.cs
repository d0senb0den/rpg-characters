﻿namespace RPGcharacters.Enums;

public enum Slot
{
    Head,
    Body,
    Legs,
    Weapon
}
