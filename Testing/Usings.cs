global using Xunit;
global using RPGcharacters.Characters;
global using RPGcharacters.Enums;
global using RPGcharacters.Equipments;
global using RPGcharacters.CustomExceptions;